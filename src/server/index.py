import json, argparse, time

import tensorflow as tf
from src.server.loader import load_graph

from flask import Flask, flash, request, redirect, url_for, send_from_directory
from flask_cors import CORS
from werkzeug.utils import secure_filename

import os

##################################################
# API part
##################################################
from src.training.utils import data_utils

app = Flask(__name__)
cors = CORS(app)

UPLOAD_FOLDER = '/storage/robots_humans/upload'
ALLOWED_EXTENSIONS = set(['wav'])


def allowed_file(filename):
    print('checking if ' + filename + ' is supported')
    isSupported = '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    print(str(isSupported))
    return isSupported


@app.route('/ai/predict', methods=['GET', 'POST'])
def predict_file():
    print('uploading the file...')

    frequency_bins = 64
    time_bins = 211

    start = time.time()
    print(request.headers)
    # check if the post request has the file part
    if 'file' not in request.files:
        print('file not found...')
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    print('filename: ' + file.filename)
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)

        print("Time spent handling upload: %f" % (time.time() - start))

        start = time.time()
        logmelspec = data_utils.file_to_logmelspec_X(filepath, time_bins, frequency_bins)
        batched_logmelspec = data_utils.single_logmelspec_to_batch_dimension(logmelspec)
        print(batched_logmelspec.shape)
        extraction_time = time.time() - start
        print("Time spent extracting features: %f" % extraction_time)

        start = time.time()
        y_out = persistent_sess.run(y, feed_dict={x: batched_logmelspec})
        prediction_time = time.time() - start
        print("Time spent predicting: %f" % prediction_time)

        json_data = json.dumps({
            'prediction': y_out.tolist()[0],
            'description': file.filename,
            'predictionTime': prediction_time,
            'extractionTime': extraction_time,
        })

        print(str(json_data))

        time.sleep(2.0)  ## artificially pause 2 seconds because we're too quick at this ;)

        return json_data
    else:
        print('no file or allowed filetype')

        json_data = json.dumps({
            'description': file.filename,
            'error': 'Wrong filetype. Please use a .wav file!'
        })

        print(str(json_data))

        return json_data


@app.route('/healtcheck', methods=['GET'])
def healthcheck():
    print('flask /healtcheck')
    return

@app.route('/ai/file/<path:filename>', methods=['GET'])
def download(filename):
    print('downloading the file...')
    uploads = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])
    return send_from_directory(directory=uploads, filename=filename)


@app.route('/ai/file', methods=['GET', 'POST'])
def upload():
    print('uploading file...')
    # check if the post request has the file part
    if 'file' not in request.files:
        print('file not found...')
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    print('filename: ' + file.filename)
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)
        return '/ai/file/' + filename


##################################################
# END API part
##################################################

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--frozen_model_filename",
                        default="results/frozen_model.pb",
                        type=str,
                        help="Frozen model file to import"
                        )

    # parser.add_argument("--gpu_memory",
    #                     default=.2,
    #                     type=float,
    #                     help="GPU memory per process"
    #                     )

    args = parser.parse_args()

    ##################################################
    # Tensorflow part
    ##################################################
    print('Loading the model')
    graph = load_graph(args.frozen_model_filename)
    x = graph.get_tensor_by_name('prefix/x:0')
    y = graph.get_tensor_by_name('prefix/y_pred:0')

    # print('Starting Session, setting the GPU memory usage to %f' % args.gpu_memory)
    # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.gpu_memory)
    # sess_config = tf.ConfigProto(gpu_options=gpu_options)
    # persistent_sess = tf.Session(graph=graph, config=sess_config)
    persistent_sess = tf.Session(graph=graph)
    ##################################################
    # END Tensorflow part
    ##################################################

    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
    app.secret_key = 'mg5aN1Ia994D3Ja2uaR8l9xa20'
    app.config['SESSION_TYPE'] = 'filesystem'

    print('Starting the API')
    app.run()
