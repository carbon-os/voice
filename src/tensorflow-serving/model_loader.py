import os, argparse

import tensorflow as tf

# The original freeze_graph function
# from tensorflow.python.tools.freeze_graph import freeze_graph

dir = os.path.dirname(os.path.realpath(__file__))


def load_graph_as(model_dir, model_name):
    VERSION = 1
    SERVE_PATH = './serve/{}/{}'.format(model_name, VERSION)

    if not tf.gfile.Exists(model_dir):
        raise AssertionError(
            "Export directory doesn't exists. Please specify an export "
            "directory: %s" % model_dir)

    checkpoint = tf.train.latest_checkpoint(model_dir)

    tf.reset_default_graph()

    with tf.Session() as sess:
        # import the saved graph
        saver = tf.train.import_meta_graph(checkpoint + '.meta')
        # get the graph for this session
        graph = tf.get_default_graph()
        sess.run(tf.global_variables_initializer())
        # get the tensors that we need
        inputs = graph.get_tensor_by_name('x:0')
        predictions = graph.get_tensor_by_name('y_pred:0')

        # create tensors info
        model_input = tf.saved_model.utils.build_tensor_info(inputs)
        model_output = tf.saved_model.utils.build_tensor_info(predictions)

        builder = tf.saved_model.builder.SavedModelBuilder(SERVE_PATH)

        # build signature definition
        signature_definition = tf.saved_model.signature_def_utils.build_signature_def(
            inputs={'inputs': model_input},
            outputs={'outputs': model_output},
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME
        )

        builder.add_meta_graph_and_variables(
            sess,
            [tf.saved_model.tag_constants.SERVING],
            signature_def_map={tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature_definition}
        )
        # Save the model so we can serve it with a model server :)
        builder.save()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", type=str, default="", help="Model folder to export")
    parser.add_argument("--prod_name", type=str, default="",
                        help="The name you want to serve as, comma separated.")
    args = parser.parse_args()

    load_graph_as(args.model_dir, args.prod_name)
