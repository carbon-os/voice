import time


class LearningSession:
    def __init__(
            self,
            run_id,
            description,
            epochs,
            batch_size,
            learning_rate,
            y_shape,
            X_shape,
            train_data_size,
            test_data_size,
            robot_test_data_size,
            robot_train_data_size
    ):
        self.run_id = run_id
        self.description = description
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.y_shape = y_shape
        self.X_shape = X_shape
        self.train_data_size = train_data_size
        self.test_data_size = test_data_size
        self.robot_train_data_size = robot_train_data_size
        self.robot_test_data_size = robot_test_data_size

        self.test_loss = []
        self.test_acc = []
        self.train_loss = []
        self.train_acc = []
        self.start_time = None
        self.run_time = None

    def start_timer(self):
        self.start_time = time.time()

    def end_timer(self):
        self.run_time = time.time() - self.start_time
