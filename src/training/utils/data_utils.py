# import resampy
import librosa
import scipy.io.wavfile as sci_wav
import os

import numpy as np
import pandas as pd

from sklearn import preprocessing

from python_speech_features import mfcc

# import matplotlib.pyplot as plt
from typing import Tuple, Any, List

from src.training.utils import wav_utils


DEFAULT_SAMPLE_RATE = 16000

def list_all_files(directory: str) -> list:
    """
    lists all files in directory, the full path of each file is included
    :param directory:
    :return: an array of file names (full path included)
    """
    filenames = os.listdir(directory)
    return [directory + "/" + f for f in filenames]


def file_to_wav(file_path: str) -> Tuple[int, List]:
    """
    converts a file to a wav file
    :param file_path:
    :return: a wav of the form (sample_rate, [amplitude, amplitude, amplitude, ...])
    ie: (4410, [0.1, 0.70, ...])
    """
    wav = sci_wav.read(file_path)

    if wav_to_samplerate(wav) != DEFAULT_SAMPLE_RATE:
        wav = librosa.load(file_path, sr=DEFAULT_SAMPLE_RATE)
        # we reverse the tuple because librosa uses the opposite convention of scipy
        return wav[1], wav[0]

    return wav


# deprecated
def y_to_integer(y: List[int]) -> int:
    """
    Translates y to an integer.
    This was used in the v1 trainer as y, but has since been deprecated. y should be an array of integers, not a single integer.

    :param file_path:
    :return: 0 or 1, where 0=robot and 1=human
    """
    return y[0]


def file_to_y(file_path: str) -> List[int]:
    """
    Translates filename to y.
    In this case y is a simple 0 or 1 classification of whether 'human' is in the filename

    :param file_path:
    :return: [0, 1] or [1, 0], where the first value represents human-ness and the second value represents robot-ness
    """
    if "robot" in os.path.basename(file_path):
        return [0, 1]
    elif "human" in os.path.basename(file_path):
        return [1, 0]
    else:
        raise RuntimeError('The given file could not be translated to y: ' + file_path)


def y_to_label(y: int) -> str:
    """
    Translates y into some human readable label.
    In this case y is a simple 0 or 1 classification of whether 'human' is in the filename

    :param y
    :return: 'robot' or 'human', where 0=robot and 1=human
    """
    if y == 0:
        return 'robot'
    elif y == 1:
        return 'human'
    else:
        raise RuntimeError('The given y value could not be translated into a label: ' + y)


def wav_to_mfcc(wav: Tuple[int, List]) -> np.ndarray:
    """
    Converts the wav to a mel-frequency cepstrum

    A representation of the short-term power spectrum of a sound, based on a linear cosine transform of a log power spectrum on a nonlinear mel scale of frequency.

    MFCCs are commonly derived as follows:

    [1] Take the Fourier transform of (a windowed excerpt of) a signal.
    [2] Map the powers of the spectrum obtained above onto the mel scale, using triangular overlapping windows.
    [3] Take the logs of the powers at each of the mel frequencies.
    [4] Take the discrete cosine transform of the list of mel log powers, as if it were a signal.
    [5] The MFCCs are the amplitudes of the resulting spectrum.

    https://en.wikipedia.org/wiki/Mel-frequency_cepstrum

    :param wav:
    :return: mfcc_features,
    """
    mfcc_features = mfcc(wav[1], wav[0])
    # d_mfcc_feat = delta(mfcc_feat, 2)
    # fbank_feat = logfbank(sig,rate)

    return mfcc_features


def wav_to_specgram(wav: Tuple[int, List]) -> np.ndarray:
    """
    Converts the wav file to a spectrogram.

    Given the type of data we're dealing with, a wav file with a sample rate of 44100Hz will come out to ~22050 frequency buckets

    :param wav:
    :return: a specgram, a 2-D array of floats
    """
    # _, (graph) = plt.subplots(1, 1)
    # specgram, frequencies, time, image = graph.specgram(wav[1], Fs=wav[0])
    # return


def wav_to_signal(wav: Tuple[int, List]) -> List:
    """
    Returns the 1st element of the wav (the wav is just a tuple) which indicates the signal.

    The signal is an array of amplitudes, measured in relative voltage fluctuation caused by the microphone.

    Each amplitude is measured at uniform interval indicated by the sample rate.

    :param wav:
    :return: an array of floats indicating the amplitudes of the wav at various points in time
    """
    return wav[1]


def wav_to_samplerate(wav: Tuple[int, List]) -> int:
    """
    Returns the sample rate of the wav file, the frequency at which the amplitudes were collected.

    :param wav:
    :return: an integer samplerate in Hz (samples per second)
    """
    return wav[0]


def clean_specgram(specgram: np.ndarray) -> np.ndarray:
    """

    :param specgram:
    :return:
    """
    # return specgram[:115, 20:]
    specgram = specgram[:115, :854]
    specgram = specgram.reshape(115, 854, 1)
    return specgram


def clean_logmelspec(logmelspec: np.ndarray, time_bins: int) -> np.ndarray:
    """

    :param specgram:
    :return:
    """
    existing_length = logmelspec.shape[0]

    # (1) if the spec is longer than we need, cut a piece out of the middle
    if existing_length > time_bins:
        midpoint = int(existing_length / 2)
        begin_cut = int(midpoint - (time_bins / 2))
        end_cut = int(midpoint + (time_bins / 2))
        logmelspec = logmelspec[begin_cut:end_cut, :]

    # (2) if the spec is shorter than we need, pad the end with zeros
    if existing_length < time_bins:
        # print('found one with fewer bins: ' + str(existing_length))
        # print('padding it to be: ' + str(time_bins) + ' by ' + str(logmelspec.shape[1]))
        zeros = np.zeros((time_bins, logmelspec.shape[1]))
        zeros[:logmelspec.shape[0], :logmelspec.shape[1]] = logmelspec
        logmelspec = zeros

    # (3) add an extra dimension (because our net expects three dimensions)
    logmelspec = logmelspec.reshape(logmelspec.shape[0], logmelspec.shape[1], 1)
    return logmelspec


#
# def resample_wav(wav: Tuple[int, List], expected_rate: int) -> Tuple[int, np.ndarray]:
#     """
#     Resamples a wav file to the given rate
#
#     :param wav:
#     :param expected_rate:
#     :return:
#     """
#     samplerate = wav_to_samplerate(wav)
#     if samplerate != expected_rate:
#         signal = wav_to_signal(wav)
#         return expected_rate, resampy.resample(signal, samplerate, expected_rate)
#     else:
#         return wav


def reduce_to_mono(raw_signal: any):
    if isinstance(raw_signal, np.ndarray) & len(raw_signal.shape) > 1:
        return np.mean(raw_signal, axis=1)
    else:
        return raw_signal


def wav_data_generator(X: List, y: List, n_samples: int = 20, sample_len: int = 44100):
    """
    This generator is going to return batchs of size <n_sample>*<sample_len>

    Params:
        dataset: Either 'train' or 'test', to choose in between them
        n_samples: amount of samples per batch
        sample_len: size of the samples in a batch
    """

    # Create two huuuges 1D arrays with all the audio waves concatenated one after the other
    # (one for the robots, the other for the humans)
    X_robot = np.concatenate([_x for _x, _y in zip(X, y) if _y == 0])
    X_human = np.concatenate([_x for _x, _y in zip(X, y) if _y == 1])

    # Apply normalization and mean suppression
    X_robot = preprocessing.scale(X_robot)
    X_human = preprocessing.scale(X_human)

    for _ in range(int(max(sum(y), len(y) - sum(y)) / n_samples)):
        y_batch = np.zeros(n_samples)
        X_batch = np.zeros((n_samples, sample_len))
        for idx in range(n_samples):
            y_batch[idx] = idx % 2
            _X = X_robot if y_batch[idx] == 0 else X_human
            x_idx = np.random.randint(len(_X) - sample_len)
            X_batch[idx] = _X[x_idx: x_idx + sample_len]

        yield (X_batch.reshape(n_samples, sample_len, 1),
               y_batch.reshape(-1, 1))


def wav_to_logmelspec(wav: Tuple[int, List], frequency_bins: int = 64, seconds_per_bin: int = 0.025):
    # Copyright (C) 2018 CarbonOS
    #
    # Based on
    #
    # Copyright (C) 2017 DataArt
    #
    # Based on
    #
    # Copyright 2016 Google Inc. All Rights Reserved.
    #
    # Licensed under the Apache License, Version 2.0 (the "License");
    # you may not use this file except in compliance with the License.
    # You may obtain a copy of the License at
    #
    #     http://www.apache.org/licenses/LICENSE-2.0
    #
    # Unless required by applicable law or agreed to in writing, software
    # distributed under the License is distributed on an "AS IS" BASIS,
    # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    # See the License for the specific language governing permissions and
    # limitations under the License.
    """Converts audio waveform into an array of examples for VGGish.

    Args:
      wav: wav data

    Returns:
      3-D np.array of shape [num_examples, num_frames, num_bands] which represents
      a sequence of examples, each of which contains a patch of log mel
      spectrogram, covering num_frames frames of audio and num_bands mel frequency
      bands, where the frame length is params.STFT_HOP_LENGTH_SECONDS.
    """
    signal = wav_to_signal(wav)
    samplerate = wav_to_samplerate(wav)

    # NUM_FRAMES = 96  # Frames in input mel-spectrogram patch.
    # NUM_BANDS = 64  # Frequency bands in input mel-spectrogram patch.
    # EMBEDDING_SIZE = 128  # Size of embedding layer.
    # MAX_FRAMES = 300

    STFT_WINDOW_LENGTH_SECONDS = seconds_per_bin
    STFT_HOP_LENGTH_SECONDS = 0.010
    NUM_MEL_BINS = frequency_bins
    MEL_MIN_HZ = 60
    MEL_MAX_HZ = 8000
    LOG_OFFSET = 0.01  # Offset used for stabilized log of input mel-spectrogram.
    EXAMPLE_WINDOW_SECONDS = 0.96  # Each example contains 96 10ms frames
    EXAMPLE_HOP_SECONDS = 0.96  # with zero overlap.

    # Compute log mel spectrogram features.
    logmelspec = wav_utils.log_mel_spectrogram(
        signal,
        audio_sample_rate=samplerate,
        log_offset=LOG_OFFSET,
        window_length_secs=STFT_WINDOW_LENGTH_SECONDS,
        hop_length_secs=STFT_HOP_LENGTH_SECONDS,
        num_mel_bins=NUM_MEL_BINS,
        lower_edge_hertz=MEL_MIN_HZ,
        upper_edge_hertz=MEL_MAX_HZ)

    # print('created logmelspec: ' + str(logmelspec.shape))

    return logmelspec

    # # Frame features into examples.
    # features_sample_rate = 1.0 / STFT_HOP_LENGTH_SECONDS
    # example_window_length = int(round(EXAMPLE_WINDOW_SECONDS * features_sample_rate))
    # example_hop_length = int(round(EXAMPLE_HOP_SECONDS * features_sample_rate))
    # log_mel_examples = wav_utils.frame(
    #     log_mel,
    #     window_length=example_window_length,
    #     hop_length=example_hop_length)
    # return log_mel_examples


def single_logmelspec_to_batch_dimension(logmelspec):
    return np.expand_dims(logmelspec, axis=0)


def file_to_logmelspec_X(file, time_bins, frequency_bins):
    wav = file_to_wav(file)
    logmelspec = wav_to_logmelspec(wav, frequency_bins)
    return clean_logmelspec(logmelspec, time_bins)


def file_to_logmelspec_X_y(file, time_bins, frequency_bins):
    X = file_to_logmelspec_X(file, time_bins, frequency_bins)
    return X, file_to_y(file)


def file_to_specgram_X(file):
    wav = file_to_wav(file)
    specgram = wav_to_specgram(wav)
    return clean_specgram(specgram)


def file_to_specgram_X_y(file):
    X = file_to_specgram_X(file)
    return X, file_to_y(file)


def specgram_data_generator(X_files: List, batch_size: int = 20):
    '''This generator is going to return batchs of size <n_sample>*<sample_len>

    Params:
        dataset: Either 'train' or 'test', to choose in between them
        n_samples: amount of samples per batch
        sample_len: size of the samples in a batch
    '''
    batch_features = np.zeros((batch_size, 115, 854, 1))
    batch_labels = np.zeros((batch_size, 2))

    batches = int(len(X_files) / batch_size)

    for batch in range(batches):
        # print("working on batch: " + str(batch))
        for batch_i in range(batch_size):
            index = (batch_size * batch) + batch_i

            file = X_files[index]
            batch_features[batch_i], batch_labels[batch_i] = file_to_specgram_X_y(file)

            # print("train file: " + str(index) + " is " + file)

        # yield batch_features, batch_labels, batch_test_features, batch_test_labels
        yield batch_features, batch_labels


def logmelspec_data_generator(X_files: List, time_bins, frequency_bins, batch_size: int = 20):
    '''This generator is going to return batchs of size <n_sample>*<sample_len>

    Params:
        dataset: Either 'train' or 'test', to choose in between them
        n_samples: amount of samples per batch
        sample_len: size of the samples in a batch
    '''
    batch_features = np.zeros((batch_size, time_bins, frequency_bins, 1))
    batch_labels = np.zeros((batch_size, 2))

    batches = int(len(X_files) / batch_size)

    for batch in range(batches):
        # print("working on batch: " + str(batch))
        for batch_i in range(batch_size):
            index = (batch_size * batch) + batch_i

            file = X_files[index]
            batch_features[batch_i], batch_labels[batch_i] = file_to_logmelspec_X_y(file, time_bins, frequency_bins)

            # print("train file: " + str(index) + " is " + file)

        # yield batch_features, batch_labels, batch_test_features, batch_test_labels
        yield batch_features, batch_labels
