import logging
import pandas as pd

from src.training.utils.learning_session import LearningSession


def configure_logger(log_file: str):
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        filename=log_file,
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.getLogger('').addHandler(console)
    return logging


def log_columns_to_csv(columns: dict, output_file: str) -> None:
    df = pd.DataFrame(dict([(k, pd.Series(v)) for k, v in columns.items()]))
    df.to_csv(output_file)


def declare_paperspace_format(log):
    log.info("{\"chart\": \"accuracy\", \"axis\": \"epoch\"}")
    log.info("{\"chart\": \"loss\", \"axis\": \"epoch\"}")


def log_to_paperspace_format(log, epoch, accuracy, loss):
    log.info("{{\"chart\": \"accuracy\", \"y\": {:.4f}, \"x\": {}}}".format(accuracy, epoch))
    log.info("{{\"chart\": \"loss\", \"y\": {:.4f}, \"x\": {}}}".format(loss, epoch))


def log_learning_session(learning_session: LearningSession, output_file: str) -> None:
    metadata = "\"{}\",\"{}\",\"{:.4f}\",\"{:.4f}\",\"{}\",\"{:.4f}\",\"{:.4f}\",\"{:.4f}\",\"{:.4f}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\n".format(
        learning_session.run_id,
        learning_session.description,
        learning_session.run_time,
        learning_session.run_time / learning_session.epochs,
        learning_session.epochs,
        learning_session.test_loss[-1],
        learning_session.test_acc[-1],
        learning_session.train_loss[-1],
        learning_session.train_acc[-1],
        learning_session.X_shape,
        learning_session.y_shape,
        learning_session.train_data_size,
        learning_session.test_data_size,
        learning_session.robot_train_data_size,
        learning_session.robot_test_data_size,
        learning_session.learning_rate,
        learning_session.batch_size,
    )

    with open(output_file, "a") as metadata_file:
        metadata_file.write(metadata)
