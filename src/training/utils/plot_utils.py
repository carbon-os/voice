import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  # Plotting

from src.training.utils import data_utils

def describe_wav_files(files):
    for file in files:
        wav = data_utils.file_to_wav(file)

        y = data_utils.file_to_y(file)
        label = data_utils.y_to_label(y)

        mfcc = data_utils.wav_to_mfcc(wav)
        mfcc_table = pd.DataFrame(mfcc)

        specgram = data_utils.wav_to_specgram(wav)
        specgram_table = pd.DataFrame(specgram)

        specgram_table.describe()
        plot_wav_as_specgram(wav, label)

        mfcc_table.describe()
        plot_mfcc(mfcc, label)


def plot_wav_as_specgram(wav, label):
    axes = plt.axes()
    signal = data_utils.wav_to_signal(wav)
    samplerate = data_utils.wav_to_samplerate(wav)
    axes.specgram(signal, Fs=samplerate)
    axes.set_title(label)
    axes.set_xlabel("Time [sec]")
    axes.set_ylabel("Frequency [Hz]")
    plt.subplots_adjust(right=2)
    plt.show()


def plot_mfcc(mfcc_features, label):
    ig, axes = plt.subplots()
    mfcc_data = np.swapaxes(mfcc_features, 0, 1)
    axes.imshow(mfcc_data, interpolation='nearest', origin='lower', aspect='auto')
    axes.set_title(label)
    plt.show()


def plot_wav(wav, label):
    fig, axes = plt.subplots()
    axes.plot(wav)
    axes.set_title(label)
    axes.set_xlabel("Samples")
    plt.subplots_adjust(hspace=0.75)
    plt.show()
