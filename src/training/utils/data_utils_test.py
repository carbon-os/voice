import numpy

from . import data_utils


def test_list_all_files():
    dir = '/carbon-os/data/voice/test_data/'
    files = data_utils.list_all_files(dir)

    assert isinstance(files, list)
    assert len(files) == 24
    for f in files:
        assert dir in f


def test_file_to_wav_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)

    assert isinstance(wav, tuple)
    assert wav[0] == 44100
    assert len(wav[1]) == 157696


def test_file_to_y_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    y = data_utils.file_to_y(file)

    assert isinstance(y, list)
    assert y == [1, 0]


def test_y_to_label_type():
    y = 1
    label = data_utils.y_to_label(y)

    assert isinstance(label, str)
    assert label == 'human'


def test_wav_to_mfcc_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)
    mfcc = data_utils.wav_to_mfcc(wav)

    assert isinstance(mfcc, numpy.ndarray)
    assert mfcc.shape == (357, 13)


def test_wav_to_specgram_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)
    specgram = data_utils.wav_to_specgram(wav)

    assert isinstance(specgram, numpy.ndarray)
    assert specgram.shape == (129, 1231)


def test_wav_to_signal_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)
    signal = data_utils.wav_to_signal(wav)

    assert isinstance(signal, numpy.ndarray)
    assert signal.shape == (157696,)


def test_wav_to_samplerate_type():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)
    samplerate = data_utils.wav_to_samplerate(wav)

    assert isinstance(samplerate, int)
    assert samplerate == 44100


def test_clean_specgram():
    file = '/carbon-os/data/voice/test_data/human_male_dan_Audio_07_10_2018_15_14_16.wav'
    wav = data_utils.file_to_wav(file)
    specgram = data_utils.wav_to_specgram(wav)

    data_utils.clean_specgram(specgram)

    print(specgram.shape)


# def test_specgram_data_generator():
#     dir = '/carbon-os/data/voice/test_data/'
#     files = data_utils.list_all_files(dir)
#     half = int(len(files) / 2)
#     batch_size = 4
#
#     count = 0
#     for X_train, y_train in data_utils.specgram_data_generator(files[:half], batch_size):
#         assert X_train.shape == (batch_size, 115, 854, 1)
#         assert y_train.shape == (batch_size, 2)
#         assert len(X_train) == batch_size
#
#     assert count == 3


def test_logmelspec_data_generator():
    dir = '/carbon-os/data/voice/test_data/'
    files = data_utils.list_all_files(dir)
    half = int(len(files) / 2)
    batch_size = 4

    count = 0

    # for file in files:
        # data_utils.wav_to_logmelspec(data_utils.file_to_wav(file))

    for X_train, y_train in data_utils.logmelspec_data_generator(files[:half], batch_size):
        # assert X_train.shape == (batch_size, 115, 854, 1)
        # assert X_test.shape == (batch_size, 115, 854, 1)
        # assert y_train.shape == (batch_size, 2)
        # assert y_test.shape == (batch_size, 2)
        # assert len(X_train) == batch_size

        print('X shape: ' + str(X_train.shape))
        print('y shape: ' + str(y_train.shape))
        print(len(X_train))

    # assert count == 3
