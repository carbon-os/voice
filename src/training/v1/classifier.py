import os

from sklearn.model_selection import train_test_split

from src.training.utils.learning_session import LearningSession

import src.training.v1.neural_net as nn

from src.training.utils import data_utils, output_utils
from src.training.v1 import learning_utils


def train(
        run_id,
        training_data_dir,
        artifact_dir,
        logger
):
    META_DATA_FILE = artifact_dir + '/metadata.csv'

    CLASSIFIER_FILE = artifact_dir + "/classifier.ckpt"
    TRAIN_TEST_SPLIT_FILE = artifact_dir + '/train_test_data_split.csv'
    TRAIN_HISTORY_FILE = artifact_dir + '/train_history.csv'

    ####
    #
    # Load data, extract labels, split it into test/train batches, extract the wav signal from the files
    #
    ####
    all_directory = os.path.join(training_data_dir, 'all')
    files = data_utils.list_all_files(all_directory)
    y_all = map(data_utils.file_to_y, files)
    y_all = list(map(data_utils.y_to_integer, y_all))

    X_train_files, X_test_files, y_train, y_test = train_test_split(files, y_all, test_size=0.33)

    train_data_size = len(y_train)
    test_data_size = len(y_test)
    robot_train_data_size = sum(y_train)
    robot_test_data_size = sum(y_test)

    # TODO: move into generator functions so they're not all loaded into memory at once
    X_train_wavs = map(data_utils.file_to_wav, X_train_files)
    X_test_wavs = map(data_utils.file_to_wav, X_test_files)

    X_train_signal = list(map(data_utils.wav_to_signal, X_train_wavs))
    X_test_signal = list(map(data_utils.wav_to_signal, X_test_wavs))

    # X_train_specgram = [data_utils.wav_to_specgram(wav) for wav in X_train_wavs]
    # X_test_specgram = [data_utils.wav_to_specgram(wav) for wav in X_test_wavs]

    ##
    #
    # build our neural net model, set hyper-parameters
    #
    ##
    logger.info('building neural net...')
    model = nn.build_wav_nn()

    ##
    #
    # train the network
    #
    ##
    logger.info('training...')
    learning_session = LearningSession(
        run_id=run_id,
        description='wav',
        epochs=25,
        batch_size=20,
        learning_rate=0.01,
        y_shape=(20, 1),
        X_shape=(20, 129, 1231),
        train_data_size=train_data_size,
        test_data_size=test_data_size,
        robot_test_data_size=robot_train_data_size,
        robot_train_data_size=robot_test_data_size
    )

    learning_session.start_timer()
    loss, acc = learning_utils.learn(
        epochs=learning_session.epochs,
        batch_size=learning_session.batch_size,
        learning_rate=learning_session.learning_rate,
        model=model,
        X=X_train_signal,
        y=y_train,
        session_file=CLASSIFIER_FILE,
        log=logger,
        is_training=True
    )
    learning_session.end_timer()

    learning_session.train_loss = loss
    learning_session.train_acc = acc

    loss, acc = learning_utils.learn(
        epochs=25,
        batch_size=learning_session.batch_size,
        learning_rate=learning_session.learning_rate,
        model=model,
        X=X_test_signal,
        y=y_test,
        session_file=CLASSIFIER_FILE,
        log=logger,
        is_training=False
    )

    learning_session.test_loss = loss
    learning_session.test_acc = acc

    ##
    #
    # log statistics and output for research reference
    #
    ##
    output_utils.log_columns_to_csv({
        'train': X_train_files,
        'test': X_test_files
    }, TRAIN_TEST_SPLIT_FILE)

    output_utils.log_columns_to_csv({
        'epoch': range(1, learning_session.epochs + 1),
        'train_acc': learning_session.train_acc,
        'train_loss': learning_session.train_loss,
        'test_loss': learning_session.test_loss,
        'test_acc': learning_session.test_acc,
    }, TRAIN_HISTORY_FILE)

    output_utils.log_learning_session(learning_session, META_DATA_FILE)
