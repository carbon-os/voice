import numpy as np

import tensorflow as tf

from src.training.utils import data_utils, output_utils


def learn(epochs, batch_size, learning_rate, model, X, y, log, is_training, session_file):
    saver = tf.train.Saver()
    loss = []
    acc = []

    with tf.Session() as sess:
        output_utils.declare_paperspace_format(log)
        if tf.train.checkpoint_exists(session_file):
            saver.restore(sess, session_file)
        else:
            sess.run(tf.global_variables_initializer())

        for e in range(epochs):
            epoch_loss, epoch_acc = run_epoch(session=sess,
                                              model=model,
                                              X=X,
                                              y=y,
                                              batch_size=batch_size,
                                              learning_rate=learning_rate,
                                              is_training=is_training)

            loss.append(np.array(epoch_loss).mean())
            acc.append(np.array(epoch_acc).mean())

            output_utils.log_to_paperspace_format(log, e + 1, acc[-1], loss[-1])

            log.info(
                "Training? {}. Epoch {}/{} Loss: {:.4f} Acc: {:.4f}".format(
                    is_training,
                    e + 1,
                    epochs,
                    loss[-1],
                    acc[-1]
                ))

        saver.save(sess, session_file)
    return loss, acc


def run_epoch(session, model, X, y, batch_size, learning_rate, is_training):
    epoch_loss = []
    epoch_acc = []

    data_generator = data_utils.wav_data_generator(X, y, batch_size)
    for batch_x, batch_y in data_generator:
        feed = {
            model.inputs: batch_x,
            model.labels: batch_y,
            model.learning_rate: learning_rate,
            model.is_training: is_training
        }
        # Do the training
        batch_loss, _, batch_acc = session.run([model.cost, model.optimizer, model.accuracy], feed_dict=feed)

        # Accumulate the resulting values
        epoch_acc.append(batch_acc)
        epoch_loss.append(batch_loss)

    return epoch_loss, epoch_acc
