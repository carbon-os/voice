import tensorflow as tf
from collections import namedtuple


def build_wav_nn():
    '''We build a 10 layer convolutional neural network ending with
    a Global average Pooling layer whose logits are submitted to
    a sigmoid function.

    Receptive fied is : U_0 = 1; U_{n+1} = U_n * 2 + 1
    Which is, for 10 layers : 2047
    '''
    tf.reset_default_graph()
    inputs = tf.placeholder(tf.float32, shape=[None, None, 1])
    labels = tf.placeholder(tf.float32, shape=[None, 1])
    learning_rate = tf.placeholder(tf.float32)
    is_training = tf.Variable(True, dtype=tf.bool)

    nn = tf.layers.conv1d(inputs,
                          filters=10,
                          kernel_size=3,
                          strides=2,
                          activation=tf.nn.relu)
    for _ in range(9):
        nn = tf.layers.conv1d(nn,
                              filters=10,
                              kernel_size=3,
                              strides=2,
                              activation=tf.nn.relu)
        nn = tf.layers.batch_normalization(nn, training=is_training)

    # Global average pooling
    nn = tf.reduce_mean(nn, [1])

    logits = tf.layers.dense(nn, 1, activation=None)
    cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits)
    cost = tf.reduce_mean(cross_entropy)

    with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    predicted = tf.nn.sigmoid(logits)
    correct_pred = tf.equal(tf.round(predicted), labels)
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Export the nodes
    export_nodes = ['inputs', 'labels', 'learning_rate', 'is_training', 'logits',
                    'cost', 'optimizer', 'predicted', 'accuracy']
    Graph = namedtuple('Graph', export_nodes)
    local_dict = locals()
    graph = Graph(*[local_dict[each] for each in export_nodes])

    return graph