import os

import tensorflow as tf

from sklearn.model_selection import train_test_split

import numpy as np

# Adding Seed so that random initialization is consistent
from numpy.random import seed

from src.server import freeze_graph
from src.training.utils import data_utils, output_utils
from src.training.v2.nn import create_nn

seed(1)
from tensorflow import set_random_seed

set_random_seed(2)


def train(
        run_id,
        training_data_dir,
        artifact_dir,
        logger
):
    META_DATA_FILE = artifact_dir + '/metadata.csv'

    CLASSIFIER_FILE = artifact_dir + "/classifier.ckpt"
    TRAIN_TENSORBOARD = artifact_dir + "/tensorboard_train"
    TEST_TENSORBOARD = artifact_dir + "/tensorboard_test"
    TRAIN_TEST_SPLIT_FILE = artifact_dir + '/train_test_data_split.csv'
    TRAIN_HISTORY_FILE = artifact_dir + '/train_history.csv'

    ####
    #
    # Load data, extract labels, split it into test/train batches, extract the wav signal from the files
    #
    ####
    all_directory = os.path.join(training_data_dir, 'all')
    files = data_utils.list_all_files(all_directory)
    y_all = list(map(data_utils.file_to_y, files))

    X_train_files, X_test_files, y_train, y_test = train_test_split(files, y_all, test_size=0.33)

    batch_size = 32
    validation_size = 100

    # Prepare input data
    # num_classes = 2
    #
    time_bins = 211
    frequency_bins = 64
    # num_channels = 1

    session = tf.Session()

    # pred_true_matrix = np.column_stack([y_true_cls, y_pred_cls])
    # tf.summary.histogram('pred_true_matrix', pred_true_matrix)

    session.run(tf.global_variables_initializer())

    x, y_true, cost, accuracy, optimizer, merged_summary = create_nn(t=time_bins, f=frequency_bins, channels=1, num_classes=2)

    train_tb_writer = tf.summary.FileWriter(TRAIN_TENSORBOARD, session.graph)
    test_tb_writer = tf.summary.FileWriter(TEST_TENSORBOARD)

    session.run(tf.global_variables_initializer())

    global total_iterations
    total_iterations = 0

    saver = tf.train.Saver()

    train_file_count = len(X_train_files)
    epochs = 20
    output_utils.declare_paperspace_format(logger)
    for epoch in range(epochs):
        logger.info("running epoch: " + str(epoch) + ' of ' + str(epochs))

        batch = 0
        for x_train_batch, y_train_batch in data_utils.logmelspec_data_generator(X_train_files, time_bins, frequency_bins, batch_size):
            logger.info('batch: ' + str(batch) + '/' + str(train_file_count / batch_size) +
                        '; epoch: ' + str(epoch) + '/' + str(epochs))

            feed_dict_tr = {
                x: x_train_batch,
                y_true: y_train_batch
            }

            summary, _ = session.run([merged_summary, optimizer], feed_dict=feed_dict_tr)
            train_tb_writer.add_summary(summary, epoch)
            batch += 1

        if epoch % 2 == 0:

            batch_test_X = np.zeros((batch_size, time_bins, frequency_bins, 1))
            batch_test_y = np.zeros((batch_size, 2))

            # batch_train_X = np.zeros((batch_size, 115, frequency_bins, 1))
            # batch_train_y = np.zeros((batch_size, 2))

            for batch_i in range(validation_size):
                index = np.random.randint(0, len(X_test_files))

                file_test = X_test_files[index]
                batch_test_X[batch_i], batch_test_y[batch_i] = data_utils.file_to_logmelspec_X_y(file_test, time_bins, frequency_bins)
                #
                # file_train = X_train_files[index]
                # batch_train_X[batch_i], batch_train_y[batch_i] = data_utils.file_to_logmelspec_X_y(file_train)

                # print("test file: " + str(index) + " is " + file_test)

            # feed_dict_tr = {
            #     x: batch_train_X,
            #     y_true: batch_train_y
            # }

            feed_dict_val = {
                x: batch_test_X,
                y_true: batch_test_y
            }

            print("showing progress...")
            val_loss = session.run(cost, feed_dict=feed_dict_val)
            acc = session.run(accuracy, feed_dict=feed_dict_tr)
            summary, val_acc = session.run([merged_summary, accuracy], feed_dict=feed_dict_val)

            test_tb_writer.add_summary(summary, epoch)

            msg = "Training Epoch {0} --- Training Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%},  Validation Loss: {3:.3f}"
            logger.info(msg.format(epoch + 1, acc, val_acc, val_loss))
            output_utils.log_to_paperspace_format(logger, epoch, val_acc, val_loss)

    saver.save(session, CLASSIFIER_FILE)

    #  we can't do this yet because it seems the architecture is different
    # freeze_graph.freeze_graph(artifact_dir, "y_pred")
