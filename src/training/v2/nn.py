import tensorflow as tf

from src.training.utils import tensorboard_utils


def create_nn(t, f, channels, num_classes):
    x = tf.placeholder(tf.float32, shape=[None, t, f, channels], name='x')

    ## labels
    y_true = tf.placeholder(tf.float32, shape=[None, num_classes], name='y_true')
    y_true_cls = tf.argmax(y_true, dimension=1)

    ##Network graph params
    filter_size_conv1 = 3
    num_filters_conv1 = 32

    filter_size_conv2 = 3
    num_filters_conv2 = 32

    filter_size_conv3 = 3
    num_filters_conv3 = 64

    fc_layer_size = 128

    layer_conv1 = create_convolutional_layer(input_layer=x,
                                             num_input_channels=channels,
                                             conv_filter_size=filter_size_conv1,
                                             num_filters=num_filters_conv1,
                                             name="layer1")

    layer_conv2 = create_convolutional_layer(input_layer=layer_conv1,
                                             num_input_channels=num_filters_conv1,
                                             conv_filter_size=filter_size_conv2,
                                             num_filters=num_filters_conv2,
                                             name="layer2")

    layer_conv3 = create_convolutional_layer(input_layer=layer_conv2,
                                             num_input_channels=num_filters_conv2,
                                             conv_filter_size=filter_size_conv3,
                                             num_filters=num_filters_conv3,
                                             name="layer3")

    layer_flat = create_flatten_layer(layer_conv3)

    layer_fc1 = create_fc_layer(input_layer=layer_flat,
                                num_inputs=layer_flat.get_shape()[1:4].num_elements(),
                                num_outputs=fc_layer_size,
                                name="fc-1",
                                use_relu=True)

    layer_fc2 = create_fc_layer(input_layer=layer_fc1,
                                num_inputs=fc_layer_size,
                                num_outputs=num_classes,
                                name="fc-2",
                                use_relu=False)

    y_pred = tf.nn.softmax(layer_fc2, name='y_pred')
    tf.summary.histogram('y_pred', y_pred)

    y_pred_cls = tf.argmax(y_pred, dimension=1, name='y_pred_class')

    with tf.name_scope('cross_entropy'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2, labels=y_true)
        # tf.summary.histogram('cross_entropy', cross_entropy)

    cost = tf.reduce_mean(cross_entropy, name='cost')
    tf.summary.scalar('cost', cost)

    with tf.name_scope('train'):
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)

    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_prediction = tf.equal(y_pred_cls, y_true_cls)
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name='accuracy')

    tf.summary.scalar('accuracy', accuracy)

    merged_summary = tf.summary.merge_all()

    return x, y_true, cost, accuracy, optimizer, merged_summary


def create_convolutional_layer(input_layer,
                               num_input_channels,
                               conv_filter_size,
                               num_filters,
                               name):
    with tf.name_scope(name):
        with tf.name_scope('weights'):
            weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
            tensorboard_utils.variable_summaries(weights)

        with tf.name_scope('biases'):
            biases = create_biases(num_filters)
            tensorboard_utils.variable_summaries(biases)

        layer = tf.nn.conv2d(input=input_layer,
                             filter=weights,
                             strides=[1, 1, 1, 1],
                             padding='SAME',
                             name="conv-conv2d")
        # tf.summary.histogram("conv-conv2d", layer)

        layer += biases

        max_pooled = tf.nn.max_pool(value=layer,
                                    ksize=[1, 2, 2, 1],
                                    strides=[1, 2, 2, 1],
                                    padding='SAME',
                                    name="max_pool")
        # tf.summary.histogram("max_pool", max_pooled)

        activations = tf.nn.relu(max_pooled, name="relu")
        tf.summary.histogram("activations", activations)

    return activations


def create_fc_layer(input_layer,
                    num_inputs,
                    num_outputs,
                    name,
                    use_relu=True):
    with tf.name_scope(name):
        with tf.name_scope('weights'):
            weights = create_weights(shape=[num_inputs, num_outputs])

        with tf.name_scope('biases'):
            biases = create_biases(num_outputs)

    # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
    matmulled = tf.matmul(input_layer, weights, name="fc-matmul") + biases
    # tf.summary.histogram("matmulled", matmulled)

    if use_relu:
        relued = tf.nn.relu(matmulled, name="fc-relu")
        # tf.summary.histogram("relued", relued)
        return relued
    else:
        return matmulled


def create_flatten_layer(layer):
    layer_shape = layer.get_shape()
    num_features = layer_shape[1:4].num_elements()
    flattened = tf.reshape(layer, [-1, num_features], name="flatten")
    # tf.summary.histogram("flatten", flattened)

    return flattened


def create_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05), name="weight")


def create_biases(size):
    return tf.Variable(tf.constant(0.05, shape=[size]), name="bias")
