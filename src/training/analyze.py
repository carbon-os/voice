import warnings

import os
import sys
import time
import datetime

import pandas as pd

from src.training.utils import output_utils, data_utils


def analyze():
    ####
    #
    # IO Overhead
    #
    ####
    warnings.filterwarnings('ignore')

    all_files = data_utils.list_all_files('/storage/robots_humans/all')

    cleaned_length_array = []
    robot_length_array = []
    human_length_array = []

    for file in all_files:
        wav = data_utils.file_to_wav(file)

        if (data_utils.wav_to_samplerate(wav) != 16000):
            print("found file with wrong samplerate: " + file + str(data_utils.wav_to_samplerate(wav)))

        logmelspec = data_utils.wav_to_logmelspec(wav, 64)

        if data_utils.file_to_y(file) == [0, 1]:
            robot_length_array.append(logmelspec.shape[0])

        if data_utils.file_to_y(file) == [1, 0]:
            human_length_array.append(logmelspec.shape[0])

        logmelspec = data_utils.clean_logmelspec(logmelspec, 249)
        cleaned_length_array.append(logmelspec.shape[0])

    robot_dataframe = pd.DataFrame({'robots': robot_length_array})
    human_dataframe = pd.DataFrame({'humans': human_length_array})
    cleaned_dataframe = pd.DataFrame({'cleaned': cleaned_length_array})

    print(robot_dataframe.describe(percentiles=[.01, .05, .1, .25, .5, .75, .9, .95, .99]))
    print(human_dataframe.describe(percentiles=[.01, .05, .1, .25, .5, .75, .9, .95, .99]))
    print(cleaned_dataframe.describe(percentiles=[.01, .05, .1, .25, .5, .75, .9, .95, .99]))


if __name__ == "__main__":
    analyze()
