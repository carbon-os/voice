import warnings

import os
import sys
import time
import datetime

from src.training.utils import output_utils


def train(environment):
    ####
    #
    # IO Overhead
    #
    ####
    warnings.filterwarnings('ignore')

    RUN_ID = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')

    # ABSOLUTE_PATH = os.path.dirname(os.path.realpath(__import__("__main__").__file__))
    DATA_DIR = os.path.join(os.sep, 'storage', 'robots_humans')
    ARTIFACT_ROOT = os.path.join(os.sep, 'artifacts', 'robots_humans', environment)

    # DATA_DIR = '../../storage/robots_humans'
    # ARTIFACT_ROOT = '../../artifacts/robots_humans/' + environment

    # ARTIFACT_RUN_DIR = ARTIFACT_ROOT + '/' + RUN_ID
    ARTIFACT_RUN_DIR = os.path.join(ARTIFACT_ROOT, RUN_ID)
    # RUN_LOG = ARTIFACT_RUN_DIR + '/classifier.log'
    RUN_LOG = os.path.join(ARTIFACT_RUN_DIR, 'classifier.log')

    if not os.path.exists(ARTIFACT_RUN_DIR):
        os.makedirs(ARTIFACT_RUN_DIR)

    logger = output_utils.configure_logger(RUN_LOG)

    ####
    #
    # Run the train method
    #
    ####

    from src.training.v2 import classifier

    classifier.train(RUN_ID, DATA_DIR, ARTIFACT_RUN_DIR, logger)


if __name__ == "__main__":
    if (len(sys.argv) != 2) or ((sys.argv[1] != 'prod') and (sys.argv[1] != 'dev')):
        raise RuntimeError("Must specify 'prod' or 'dev' as the only commandline arg")
    environment = sys.argv[1]
    train(environment)
