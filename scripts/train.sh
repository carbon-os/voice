#!/usr/bin/env bash

if [[ -z $1 ]] ; then
    echo "Please specify 'dev' or 'prod'"
    echo "ie. ./scripts/train.sh dev"
    exit 1
fi

python src/training/train.py $1
