#!/usr/bin/env bash
#!/usr/bin/env bash

if [ ! -d /artifacts ]; then
 echo "Please create a local directory /artifacts and set it's permissions before using this script. See the README instructions"
 exit 0;
fi

JOB_ID=$1

if [ -z "$JOB_ID" ]; then
  echo "Please specify the job ID"
  echo "Example: ./download_artifacts.sh jy9nqxupjoeuu"
  exit 1
fi

cd /artifacts

paperspace jobs artifactsGet --jobId $JOB_ID