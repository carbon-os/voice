#!/usr/bin/env bash

JOB_ID=$(sh scripts/paperspace/oldest_job_id.sh)

echo "destroying job ${JOB_ID}... (an empty JSON response indicates success)"

paperspace jobs destroy --jobId=${JOB_ID}