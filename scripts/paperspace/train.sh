#!/usr/bin/env bash

DATE_WITH_TIME=`date "+%Y%m%d-%H%M%S"`

paperspace jobs create --container danpkbar/carbon-os-trainer:1.1 --machineType P4000 --region "East Coast (NY2)" --command "export DISPLAY=:0.0 && export PYTHONPATH=. && /paperspace/scripts/train.sh dev"