#!/usr/bin/env bash

JOBS_RAW=$(paperspace jobs list | grep "\"id\":")

RAW_ARRAY=(${JOBS_RAW//\"/ })

RAW_LENGTH=${#RAW_ARRAY[@]}

NEWEST_INDEX="$((RAW_LENGTH - 2))"

echo ${RAW_ARRAY[$NEWEST_INDEX]}