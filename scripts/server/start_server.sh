#!/usr/bin/env bash

ENV=$1
if [ -z "$ENV" ]; then
    echo "Please specify the environment"
    echo "ie:  ./start_server.sh axon-stage 20180813095434"
    exit 0
fi

MODEL_ID=$2
if [ -z "$MODEL_ID" ]; then
    echo "Please specify the model id"
    echo "ie:  ./start_server.sh axon-stage 20180813095434"
    exit 0
fi

echo "Starting server on " ${ENV}
ssh -t -t ${ENV} "cd /carbon-os/target/voice && ./start.sh ${MODEL_ID}"
