#!/usr/bin/env bash

ENV=$1
if [ -z "$ENV" ]; then
    echo "Please specify the environment"
    echo "ie:  ./stop_server.sh axon-stage"
    exit 0
fi

echo "Stopping server on " ${ENV}
ssh -t -t ${ENV} "cd /carbon-os/target/voice && ./stop.sh"
