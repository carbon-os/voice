#!/usr/bin/env bash

# Validate presence of command line parameters
ENV=$1
if [ -z "$ENV" ]; then
  echo "Specify the target server alias"
  echo "ie: ./deploy_model.sh axon-stage 20180813095434"
  exit 1
fi

# Validate presence of command line parameters
MODEL_ID=$2
if [ -z "$MODEL_ID" ]; then
  echo "Specify the model you would like to deploy"
  echo "ie: ./deploy_model.sh axon-stage 20180813095434"
  exit 1
fi

if [ ! -d "/artifacts/robots_humans/prod/${MODEL_ID}" ]; then
    echo "/artifacts/robots_humans/prod/${MODEL_ID} does not exist!"
    exit 1
fi

MODEL_SRC_DIR=/artifacts/robots_humans/prod/${MODEL_ID}
MODEL_TARGET_DIR=/artifacts/robots_humans/prod

rsync -rcav --perms --chmod=a+r,u+w -e ssh ${MODEL_SRC_DIR} ${ENV}:${MODEL_TARGET_DIR}/
