#!/usr/bin/env bash

FLASK_SRC_DIR=/carbon-os/workspace/voice
FLASK_TARGET_DIR=/carbon-os/target

# Validate presence of command line parameters
ENV=$1
if [ -z "$ENV" ]; then
  echo "Specify the target server alias"
  echo "ie: ./deploy_server.sh axon-stage"
  exit 1
fi

rsync -rcav --perms --chmod=a+r,u+w -e ssh ${FLASK_SRC_DIR} ${ENV}:${FLASK_TARGET_DIR}/
