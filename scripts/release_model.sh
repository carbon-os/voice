#!/usr/bin/env bash

# Validate presence of command line parameters
MODEL_ID=$1
if [ -z "$MODEL_ID" ]; then
  echo "Specify the model you would like to mark as prod"
  echo "ie: ./release_model.sh 20180813095434 Avogadro"
  exit 1
fi

RELEASE_NAME=$2
if [ -z "$RELEASE_NAME" ]; then
  echo "Specify the name for the new prod model"
  echo "ie: ./release_model.sh 20180813095434 Avogadro"
  exit 1
fi

cp -r /artifacts/robots_humans/dev/$MODEL_ID/frozen_model.pb /artifacts/robots_humans/prod/${RELEASE_NAME}/frozen_model.pb

#cp -r /artifacts/robots_humans/dev/$MODEL_ID/* /artifacts/robots_humans/prod/${MODEL_ID}

#Bell
#Curie
#Dalton
#Euler
#Faraday
#Goddard
#Hawking
#Joule
#Kepler
#Lorentz
#Marconi
#Newton
#Ohm
#Poincare
#Quinn
#Rutherford
#Schrödinger
#Tesla
#...
