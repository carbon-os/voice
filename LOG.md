###
Monday, Aug 13, 2018
Finished Angular App v0.1: ability to bulk upload wav files, hit "Analyze" and get a result

Tuesday, Aug 14, 2018
- Trained 100 epochs on recorded data, getting 75% results

Wednesday, Aug 15, 2018
- Worked for a while on Record-in-Browser; could not get it working, saved on branch
- Made a Google Cloud Account, got API key

Thursday Aug 16, 2018
- Wrote translation flac -> wav for existing datasets
- Got single download working for google cloud
- Found a dictionary of sentences and downloaded it for random sentences

Friday Aug 17, 2018
- wrote normal distribution code for random pitch/volume/speed
- Cleaned dictionary, downloaded 5000 Google Wavenet samples from API
- Wrote "logmelspec clean" function which pads short samples and trims long ones
- zipped and uploaded 