# silverbox

# Development Environment

## Dependencies
git
python 3.6
[paperspace CLI](https://paperspace.zendesk.com/hc/en-us/articles/115004356414-1-Install-Paperspace-CLI)


# Developer Machines Setup


Add storage and artifacts directories if you want to run locally
```bash
sudo mkdir /storage
sudo chown ${whoami}:${whoami} /storage

sudo mkdir /artifacts
sudo chown ${whoami}:${whoami} /artifacts
```


## Development Lifecycle

Upload data to paperspace:
```bash
./scripts/upload_training_data.sh
```

Train on paperspace:
```bash
./scripts/paperspace/train.sh
```

if you need to delete old jobs to train new ones, use:
```bash
./scripts/paperspace/oldest_job_id.sh
```

See newest job ID:
```bash
./scripts/paperspace/newest_job_id.sh
```

Download artifacts:
```bash
./scripts/paperspace/download_artifacts.sh jv6rvqtv4w36o
```

Freeze graph (step goes away when we build this in):
```bash
export PYTHONPATH=. && python src/server/freeze_graph.py --model_dir=/artifacts/robots_humans/dev/20180813095434 --output_node_names=y_pred
```

Freeze graph (for TS):
```bash
export PYTHONPATH=. && python src/tensorflow-serving/model_loader.py --model_dir=/artifacts/robots_humans/dev/20180813095434 --prod_name=CURIE
```

Run tensorflow-serving:
```bash
docker run -it -p 9000:9000 --name tf-serve -v /carbon-os/workspace/voice/serve/:/serve/ epigramai/model-server:light --port=9000  --model_name=CURIE --model_base_path=/serve/CURIE
```

Run server:
```bash
export PYTHONPATH=. && python src/server/index.py --frozen_model_filename=/artifacts/robots_humans/prod/BELL/frozen_model.pb
```

Look at tensorboard:
```bash
tensorboard --logdir=/artifacts/robots_humans/dev/20180813095434/tensorboard_test/
## OR ##
tensorboard --logdir=/artifacts/robots_humans/dev/20180813095434/tensorboard_train/
```

## To Deploy
```bash
./scripts/release_model.sh 20180813095434 BELL
./scripts/server/stop_server.sh axon-stage
./scripts/server/deploy_model.sh axon-stage BELL
./scripts/server/deploy_server.sh axon-stage
./scripts/server/start_server.sh axon-stage BELL
```
