#!/usr/bin/env bash

MODEL_ID=$1

if [ -z "${MODEL_ID}" ]; then
  echo "Please specify the model version"
  echo "Example: ./start.sh AVOGADRO"
  exit 1
fi

export PYTHONPATH=.

forever start \
    --pidFile /carbon-os/target/voice/server.pid \
    -a \
    -l /carbon-os/logs/voice/forever.log \
    -o /carbon-os/logs/all.log \
    -e /carbon-os/logs/all.log \
    -c python src/server/index.py --frozen_model_filename=/artifacts/robots_humans/prod/${MODEL_ID}/frozen_model.pb
